package com.example.demo;

import com.example.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DemoApplication implements CommandLineRunner {

	@Autowired
	DemoService demoService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		sendThreeRequests(5);

		sendThreeRequests(10);

		invalidate(5);

		sendThreeRequests(5);

		sendThreeRequests(10);
	}

	private void sendThreeRequests(double key) {
		System.out.println();
		System.out.println("--- Drei Requests für den Eingabewert " + key + " ---");
		for(int i=0; i<3; i++) {
			System.out.println(demoService.get(key));
		}
	}

	private void invalidate(double key) {
		System.out.println();
		System.out.println("--- Cache für den Eingabewert " + key + " leeren ---");
		demoService.remove(key);
	}
}
