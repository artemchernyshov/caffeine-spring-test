package com.example.demo.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author chernyshov
 * @version 08.04.2019
 * @since 1.8
 */
@Service
@CacheConfig(cacheNames = "default")
public class DemoService {

    @Cacheable
    public double get(double key) {
        System.out.print("Teurer Request: ");
        return Math.pow(key, 2);
    }

    @CacheEvict(key = "#key")
    public void remove(double key) {
        System.out.println("Cache für Eingabewert " + key + " geleert");
    }
}
